Ddoc

$(COZUM_BOLUMU Das hello world Programm)

$(OL

$(LI

---
import std.stdio;

void main() {
    writeln("Etwas Anderes... :p");
}
---

)

$(LI

---
import std.stdio;

void main() {
    writeln("Eine Zeile");
    writeln("Noch eine Zeile!");
}
---

)

$(LI

Das folgende Programm kann nicht erstellt werden da das Semikolon fehlt:

---
import std.stdio;

void main() {
    writeln("Hello world!")    $(DERLEME_HATASI)
}
---
)

)

Macros:
        SUBTITLE=Das Hello World Programm: Lösungen

        DESCRIPTION=Lösungen für das Hello World Programm

        KEYWORDS=programming in d tutorial hello world program lösung german
