Ddoc

$(DIV_CLASS page_one,

$(DERS_BOLUMU $(IX hello world) Das Hello World Program)

$(P
Das erste Programm in den meinsten Programmierhandbüchern ist das $(I hello world) Programm.
Es ist ein sehr kleines Programm welches gerade mal "hello world" ausgibt und sich dann beendet.
Dieses Programm ist so wichtig, da es die grundlegende Struktur der Sprache veranschaulicht.
)

$(P
So schaut dieses $(I hello world) Programm in D aus:
)

---
import std.stdio;

void main() {
    writeln("Hello world!");
}
---

$(P
Der $(I Quellcode) von oben muss jetzt nur noch von einem D-Compiler Compiliert werden.
)

$(H5 $(IX compiler installation) $(IX installation, compiler) Compiler Installation)

$(P
$(IX gdc) $(IX ldc) Zurzeit gibt es drei Compiler zur Auswahl: $(C dmd), der Digital Mars Compiler; $(C gdc), der D Compiler aus der GCC; und $(C ldc), ein D compiler spezialisiert auf
die LLVM Compiler Infrastruktur.
)

$(P
$(IX dmd) $(C dmd) ist der D Compiler, welcher als Referenz für die Sprache verwendet wird. Alle Beispiele in diesem Buch wurden mit $(C dmd) getestet und daher ist es am einfachsten dmd zu installieren
und nur bei Bedarf andere auszuprobieren. Die Code-Beispiele wurden mit $(C dmd) Version 2.069 getestet.
)

$(P
Um die neueste $(C dmd) Version zu installieren besuche zuerst $(LINK2 http://www.dlang.org/download.html, die Download-Seite)
und wähle den Compiler aus, welcher deinem Computer entspricht. Wichtig dabei ist das Betriebssystem und ob du ein 32- oder 64-Bit Chipset besitzt.
Installiere bitte keinen D1 Compiler. Dieses Buch ist nur für $(I D Version 2).
)

$(P
Der Installationsvorgang ist auf jedem Computer anders aber es sollte nicht viel schwerer sein als einfachen Anweisungen am Bildschirm zu folgen.
)

$(H5 $(IX quellcode) Quellcode)

$(P
Die Datei die ein Programmierer schreibt, welche dann vom Compiler "kompiliert"(=übersetzt in Maschinensprache) wird, ist eine sogenannte $(I Quellcode Datei).
D wird zu 99% als eine Kompilierte Sprache verwendet. Die Quellcode Datei ist also kein ausführbares Programm - Sie muss erst vom Compiler in eines umgewandelt werden.
)

$(P
Wie jede Datei muss auch die Quellcode Datei einen Namen haben. Auch wenn der Name alles sein kann was das Dateisystem erlaubt, ist es üblich die $(C .d) $(I Dateiendung) für D
Quellcode zu verwenden. So weiß jedes Programm, jeder Programmierer das es sich um eine D datei handelt. Beispielsweise sind $(C test.d), $(C game.d), $(C invoice.d),usv.
übliche Namen für D Quellcode.
)

$(H5 Erstellen des Hello World Programms)

$(P
$(IX text editor) $(IX editor, text) Du wirst den Quellcode in einem $(LINK2 http://wiki.dlang.org/Editors, text editor) (oder einem $(I IDE) wie unten erwähnt) schreiben.
Schreibe oder Kopiere das hello world Programm von oben in ein Textdokument und speichere es unter dem Namen $(C hallo.d).
)

$(P
Der Compiler wird bald überprüfen ob die Syntax des Codes richtig ist (er entspricht den Regeln der Sprache) und erstellt ein Programm daraus indem er den Quellcode in
Maschinensprache übersetzt. Folge den Schritten um dein Programm zu Erstellen!
)

$(OL

$(LI Öffne ein Eingabefenster.)

$(LI Begebe dich zu dem Ort an welchem du $(C hallo.d) gespeichert hast.)

$(LI Geb folgenden Befehl ein. (Das $(C $) Zeichen musst du nicht eingeben, es dient nur dazu zu symbolisieren dass es sich um einen Konsolenbefehl handelt))

)

$(SHELL
$(SHELL_OBSERVED $) dmd hallo.d
)

$(P
Wenn du keinen fehler gemacht hast, wirst du dir jetzt denken dass nichts passiert ist. Ganz im Gegenteil! Das bedeutet das alles erfolgreich gelaufen ist. Nun sollte es sich
eine ausführbare Datei $(C hallo) (oder $(C hallo.exe) auf Windows) in diesem Ordner befinden. Dieses wurde soeben vom Compiler erstellt.
)

$(P
Sollte der Compiler jedoch irgendwelche Fehlermeldungen ausgeben, hast du wahrscheinlich einen Fehler beim abschreiben gemacht. Versuche den Fehler zu finden, ihn auszubessern und
versuche nochmals das Programm zu kompilieren. Beim Programmieren gehören Fehler zur Routine, der Vorgang des Ausbesserns und nochmals Kompilieren wird dir also noch vertraut werden.
)

$(P
Sobald das Programm erfolgreich erstellt wurde, geb den Namen der ausführbaren Datei an um sie zu starten ($(C ./hallo) unter Linux/OSX; $(C hallo.exe) unter Windows)
Du solltest auf deiner Kommandozeile nun "Hello World!" sehen.
)

$(SHELL
$(SHELL_OBSERVED $) ./hello     $(SHELL_NOTE ausführen des Programms)
Hello world!  $(SHELL_NOTE die Ausgabe des Programms)
)

$(P
Glückwunsch! Dein erstes D Programm funktioniert wie erwartet.
)

$(H5 $(IX compiler switch) Compiler Optionen)

$(P
Der Compiler hat viele Optionen welche beinflussen wie das Programm kompiliert wird. Um eine Liste aller dieser $(I Switches) zu sehen, gib nur den Namen
des Compilers ein:
)

$(SHELL
$(SHELL_OBSERVED $) dmd    $(SHELL_NOTE nur der Name, keine Datei)
DMD64 D Compiler v2.069.0
Copyright (c) 1999-2015 by Digital Mars written by Walter Bright
Documentation: http://dlang.org/
Config file: /etc/dmd.conf
Usage:
  dmd files.d ... { -switch }

  files.d        D source files
...
  -de            show use of deprecated features as errors (halt compilation)
...
  -unittest      compile in unit tests
...
  -w             warnings as errors (compilation will halt)
...
)

$(P
Die verkürzte Ausgabe von oben zeigt nur die Optionen die ich dir empfehle zu verwenden. Es macht zwar keinen Unterschied bei dem hello world programm in diesem Kapitel aber der
folgende Befehl würde das Programm erstellen, Tests durchführen und keine $(I deprecated) (= bald nicht mehr verfügbar) Funktionen oder sonstige Warnungen erlauben.
Wir werden diese und andere Optionen detaillierter in anderen Kapiteln behandeln:
)

$(SHELL
$(SHELL_OBSERVED $) dmd hallo.d -de -w -unittest
)

$(P
Die vollständige Liste aller $(C dmd) Optionen kann in der $(LINK2 http://dlang.org/dmd-linux.html, DMD Compiler Dokumentation) gefunden werden.
)

$(P
Eine weitere nützliche Option ist $(C -run). Verwendet man diese Option, wird das Programm direkt nach der Kompilierung ausgeführt:
)

$(SHELL
$(SHELL_OBSERVED $) dmd $(HILITE -run) hallo.d -w -unittest
Hello world!  $(SHELL_NOTE das Programm wurde sofort ausgeführt)
)

$(H5 $(IX IDE) IDE)

$(P
Zusätzlich zu einem Compiler kannst du auch ein IDE (integrated development environment) installieren. IDEs helfen dir bei der Programmentwicklung insofern dass sie dir
einen Texteditor, Compiler und Debugger in einem bieten.
)

$(P
Solltest du ein IDE installieren, kann das erstellen und ausführen so leicht sein wie einen Knopf zu drücken. Ich empfehle trotzedem das du dich zuerst mit den Schritten des
manuellen kompilieren auf der Komandozeile vertraut machst.
)

$(P
Solltest du dich für ein IDE entscheiden, besuche die $(LINK2 http://wiki.dlang.org/IDEs, IDE Seite auf dlang.org) um eine Liste aller D IDEs zu sehen.
)

$(H5 Bestandteile des hello world Programms)

$(P
Hier ist eine kurze liste von vielen D Konzepten die in dem kurzen Programm vorkamen:
)

$(P $(B Grundfunktionen): Jede Sprache ist definiert durch ihre Syntax, Datentypen, Schlüsselwörter, Regeln usw.
All das bilden die $(I Grundfunktionen) der Sprache. Die Klammern, Semikolons und Wörter wie $(C main) und $(C void) sind alle nach den Regeln von D platziert.
Sie sind ähnlich den Regeln in der Deutschen Sprache: Nomen, Verben, Punktierung, Grammatik usw.
)

$(P $(B Bibliotheken und Funktionen):
Die Grundfunktionen definieren nur die Struktur der Sprache. Sie werden verwendet um eigene Funktionen und Datentypen zu erstellen, welche wiederum verwendet werden
um Bibliotheken zu erstellen. Bibliotheken sind Sammlungen an wiederverwendbaren Teilen aus Programmen welche mit deinem Programm $(I verlinkt) werden um ihre Funktion zu erzielen.
)

$(P
$(C writeln) ist eine $(I Funktion) aus der D Standard Bibliothek ($(C std) = standard library). Sie wird verwendet um eine Zeile Text auszugeben
wie der Name schon verräat: write line.
)

$(P $(B Module): Die Inhalte einer Bibliothek sind in Module gruppiert um Ordnung zu bewahren.
Das einzige $(I Modul) welches in unserem hello world Programm verwendet wird ist $(C std.stdio), welches die Eingabe und Ausgabe von Daten behandelt.
)

$(P $(B Character und Strings): Ausdrücke wie $(STRING "Hello world!") werden $(I strings) genannt. Strings wiederum bestehen aus Characters (=Zeichen).
Der einzige String in unserem Programm ist $(STRING "Hello world!") und beinhaltet unter anderem $(STRING 'H'), $(STRING 'e'), $(STRING '!').
)

$(P $(B Ausführreihenfolge): Programme erfüllen ihre Aufgaben nach einer bestimmten Reihenfolge.
Diese Aufgaben starten mit den Operationen welche in der Funktion namens $(C main) geschrieben sind.
Die einzige Operation in diesem Programm gibt "Hello world!" aus.
)

$(P $(B Wichtigkeit von Groß- und Kleinschreibung): Du kannst innerhalb von Strings alles schreiben was du willst. In deinem Programm jedoch musst du dich an die Groß- und
Kleinschreibung halten.
Das ist ein wichtiger Faktor in D. Beispielsweise sind $(C writeln) und $(C Writeln) zwei verschiedene Funktionen.
)

$(P
$(IX Schlüsselwörter) $(B Schlüsselwörter): Spezielle Wörter welche Teil der Grundfunktionen sind bilden die $(I Schlüsselwörter).
Diese Wörter sind für die Sprache selbst reserviert und können nicht für andere Zwecke verwendet werden.
In unserem Programm befinden sich zwei Schlüsselwörter: $(C import), welches verwendet wird um ein Modul einzubinden; und $(C void), was hier bedeutet "hat kein Ergebnis/gibt nichts zurück".
)

$(P
Die vollständige Liste aller Schlüsselwörter ist $(C abstract), $(C alias), $(C align), $(C asm), $(C assert), $(C auto), $(C body), $(C bool), $(C break), $(C byte), $(C case), $(C cast), $(C catch), $(C cdouble), $(C cent), $(C cfloat), $(C char), $(C class), $(C const), $(C continue), $(C creal), $(C dchar), $(C debug), $(C default), $(C delegate), $(C delete), $(C deprecated), $(C do), $(C double), $(C else), $(C enum), $(C export), $(C extern), $(C false), $(C final), $(C finally), $(C float), $(C for), $(C foreach), $(C foreach_reverse), $(C function), $(C goto), $(C idouble), $(C if), $(C ifloat), $(C immutable), $(C import), $(C in), $(C inout), $(C int), $(C interface), $(C invariant), $(C ireal), $(C is), $(C lazy), $(C long), $(C macro), $(C mixin), $(C module), $(C new), $(C nothrow), $(C null), $(C out), $(C override), $(C package), $(C pragma), $(C private), $(C protected), $(C public), $(C pure), $(C real), $(C ref), $(C return), $(C scope), $(C shared), $(C short), $(C static), $(C struct), $(C super), $(C switch), $(C synchronized), $(C template), $(C this), $(C throw), $(C true), $(C try), $(C typedef), $(C typeid), $(C typeof), $(C ubyte), $(C ucent), $(C uint), $(C ulong), $(C union), $(C unittest), $(C ushort), $(C version), $(C void), $(C volatile), $(C wchar), $(C while), $(C with), $(C __FILE__), $(C __MODULE__), $(C __LINE__), $(C __FUNCTION__), $(C __PRETTY_FUNCTION__), $(C __gshared), $(C __traits), $(C __vector), und $(C __parameters).
)

$(P
$(IX asm) $(IX __vector) $(IX delete) $(IX typedef) $(IX volatile) $(IX macro) Wir werden uns, mit Ausnahme von
$(LINK2 http://dlang.org/statement.html#AsmStatement, $(C asm)) und $(LINK2 http://dlang.org/phobos/core_simd.html#.Vector, $(C __vector)) da sie den Rahmen dieses Buchs sprengen würden,
alle Schlüsselwörter in späteren Kapiteln anschauen.
$(C delete), $(C typedef), und $(C volatile) werden bald nichtmehr vorhanden sein; und $(C macro) wird zurzeit von D nicht verwendet.
)

$(PROBLEM_COK

$(PROBLEM Lass das Programm etwas anderes ausgeben.)

$(PROBLEM Ändere das Programm so, dass es mehrere Zeilen ausgibt. Dies geschieht durch das hinzufügen von einem oder mehreren $(C writeln) im Programm)

$(PROBLEM Versuche das Programm mit manchen änderungen zu Kompilieren; entferne zum Beispiel das Semikolon am Ende von $(C writeln) und beobachte einen Compiler-Fehler.
)

)

)

$(Ergin)

Macros:
        SUBTITLE=Das Hello World Programm

        DESCRIPTION=Das erste D Programm: Hello World!

        KEYWORDS=d programming language tutorial book german

SOZLER=
