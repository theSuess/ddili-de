Ddoc

$(DERS_BOLUMU Zuweisung und Auswertungsreihenfolge)

$(P
Dies sind die ersten zwei Momente wo es, bei den Meisten, etwas braucht bis es *Klick* macht.
)

$(H5 The assignment operation)

$(P
Du wirst Zeilen wie diese in so gut wie jedem Programm sehen:
)

---
    a = 10;
---

$(P
Die Bedeutung dieser Zeile ist "gebe $(C a) den Wert 10". Ännlich dem bedeutet folgendes "gebe $(C b) den Wert 20":
)

---
    b = 20;
---

$(P
Wenn du das jetzt weist, was bedeutet das:
)

---
    a = b;
---

$(P
Unglücklicherweise ist es nicht wie in der Mathematik. Der Ausdruck bedeutet $(C nicht) "a ist b" sondern wie bei genauer Beobachtung klar wird: "gebe $(C a) den Wert von $(C b)".
)

$(P
Das allbekannte $(C =) Symbol hat in der Programmierung eine ganz andere Bedeutung: weise der linken Seite den Wert der Rechten zu.
)

$(H5 Auswertungsreihenfolge)

$(P
Generell gesehen werden Operationen Schritt für Schritt ausgeführt wie sie im Quellcode stehen. (Es gibt ausnahmen zu dieser Regel welche wir in späteren Kapiteln behandeln.)
)

---
    a = 10;
    b = 20;
    a = b;
---

$(P
Die Bedeutung dieser drei Zeilen ist: "gebe $(C a) den Wert 10 $(C dann) gebe $(C b) den Wert 20 $(I dann) gebe $(C a) den Wert von $(C b)".
Am Schluss haben also sowohl $(C b) als auch $(C a) den Wert 20
)

$(PROBLEM_TEK

$(P
Versuche zu verstehen warum folgende drei Operationen die Werte von $(C a) und $(C b) vertauschen. Wenn am Anfang ihre Werte 1 und 2 sind, sind sie danach 2 und 1:
)

---
    c = a;
    a = b;
    b = c;
---

)

$(Ergin)

Macros:
        SUBTITLE=Zuweisung und Auswertungsreihenfolge

        DESCRIPTION=Zwei problematische Konzepte

        KEYWORDS=d programming language tutorial buch german

MINI_SOZLUK=
