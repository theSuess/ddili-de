Ddoc

$(DERS_BOLUMU $(IX writeln) $(IX write) $(CH4 writeln) und $(CH4 write))

$(P
Im vorherigen Kapitel haben wir gesehen das $(C writeln) einen String in Hochkommas (Anführungszeichen) übergeben bekommt
und diesen dann auf der Konsole ausgibt.
)

$(P
$(C writeln) ist eine sogenannte $(I Funktion) und der String, welcher ihr übergeben wird, ist ein $(I Parameter).
Das ganze wird dann das $(I übergeben von Parameterwerten an eine Funktion) genannt. Parameter befinden sich in den
Klammern der Funktion und sind mit Kommas getrennt.
)

$(P
$(I $(B Zur Information:) Das Wort ) Parameter $(I beschreibt die Konzeptuelle information welche der Funktion übergeben wird. Die Konkrete Information welche der Funktion übergeben wird ist ein) Argument.
$(I Auch wenn sie technisch gesehen nicht das Selbe bedeuten, werden sie oft gleichbedeutend verwendet.)
)

$(P
$(C writeln) kann mehr als ein Argument übernehmen. Es gibt alle Argumente nacheinander auf einer Zeile aus:
)

---
import std.stdio;

void main() {
    writeln("Hello world!", "Hallo du!");
}
---

$(P
Manchmal ist noch nicht alle Information welche für die Ausgabe benötigt wird zur Zeit vom Aufruf von $(C writeln) verfügbar.
In solchen Fällen kann der erste Teil mit $(C write) begonnen werden und am Schluss mit $(C writeln) beendet werden.
)

$(P
$(C writeln) geht nach dem Aufruf in die nächste Zeile, $(C write) bleibt auf der selben:
)

---
import std.stdio;

void main() {
    // Wir geben als erstes aus was wir schon zur verfügung haben
    write("Hello");

    // ... nehmen wir an wir berechnen irgendetwas hier ...

    write("world!");

    // ... und schlussendlich:
    writeln();
}
---

$(P
Ein Aufruf von $(C writeln) ohne Parameter vervollständigt die aktuelle Zeile oder gibt eine leere Zeile aus, wenn in der aktuellen Zeile nichts steht.
)

$(P
$(IX //) $(IX Kommentar) Zeilen die mit $(COMMENT //) beginnen sind sogenannte  $(I Kommentar-Zeilen) oder kurz gesagt $(I Kommentare).
Ein Kommentar beeinflusst das verhalten des Programms nicht. Sein einziger Sinn ist es, zu erklären was der Code genau macht.
Der Kommentar richtet sich an Programmierer welche das Programm später einmal lesen werden. Auch der Programmierer welcher es schrieb ist damit gemeint.
)

$(PROBLEM_COK

$(PROBLEM

Beide Programme in diesem Kapitel geben den Text ohne Leerzeichen zwischen den Strings aus. Ändere die Programme so, dass ein Abstand zwischen den Parametern/der Ausgabe ist.

)

$(PROBLEM
Versuche ebenso $(C write) mit mehr als einem Parameter aufzurufen.
)

)


Macros:
        SUBTITLE=writeln und write

        DESCRIPTION=Zwei Funktionen aus der D Standard-Bibliothek: writeln und write.

        KEYWORDS=d programming language tutorial buch german writeln write

MINI_SOZLUK=
