Ddoc

$(DERS_BOLUMU $(IX standard input) $(IX standard output) Standard Input und Output Streams)

$(P
Bisher ist unsere Ausgabe immer im $(I Terminal) erschienen. Auch wenn das Terminal so gut wie immer das Ziel der Ausgabe ist, muss es nicht sein.
Die Objekte welche die Ausgabe übernehmen sind $(I standard output streams).
)

$(P
Standard output basiert auf $(I characters). Die Zahl 100, welche wir im vorherigen Kapitel ausgegeben haben werden dem Stream nicht als Zahl übergeben sondern als einzelne
Zeichen $(C 1) $(C 0) $(C 0).
)

$(P
Ähnlich dem ist die Tastatur der $(I Standard input stream). Dieser ist ebenso character basiert. Die eingegebene Zahl 42 wird also als $(C 4) $(C 2) übergeben.
)

$(P
Diese Umwandelungen passieren automatisch.
)

$(P
Das Konzept der aufeinanderfolgenden Zeichen ist ein $(I character stream). D's standard input und output sind ebenso character streams.
)

$(P
$(IX stdin) $(IX stdout) In D heißen input und output stream $(C stdin) und $(C stdout).
)

$(P
Normalerweise benötigt man um auf die Streams zuzugreifen den Namen, einen Punkt und die Operation; wie beispielsweise $(C stream.operation()).
Da $(C stdin) und $(C stdout) sehr häufig verwendet werden, können sie auch ohne namen und Punkt aufgerufen werden.
)

$(P
Die Funktion $(C writeln) kann auch als $(C stdout.writeln()) geschrieben werden. Das selbe gilt für $(C write). Folgedessen können wir das hello world Programm auch so schreiben:
)

---
import std.stdio;

void main() {
    $(HILITE stdout.)writeln("Hello world!");
}
---

$(PROBLEM_TEK

$(P
Beobachte dass sich $(C stdout.write) genau so wie $(C write) verhält.
)

)

Macros:
        SUBTITLE=Standard Input and Output Streams

        DESCRIPTION=D's standard input und output streams

        KEYWORDS=d programming language tutorial buch stdin stdout german
