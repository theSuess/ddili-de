Ddoc

$(COZUM_BOLUMU Eingabe aus dem Standard-Input lesen)

$(P
Wenn die Eingabe nicht konvertiert werden kann nimmt $(C stdin) einen Zustand an in welchem der Stream nicht verwendet werden kann.
)

Macros:
        SUBTITLE=Eingabe aus dem Standard-Input lesen

        DESCRIPTION=Programming in D Lösung: Eingabe aus dem Standard-Input lesen
        KEYWORDS=programming in d tutorial eingabe lesen stdin lösung
