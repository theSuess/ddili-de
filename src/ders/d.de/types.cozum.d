Ddoc

$(COZUM_BOLUMU Fundamentale Datentypen)


$(P
Statt $(C int) können wir einfach andere Typen verwenden.
)

---
import std.stdio;

void main() {
    writeln("Datentyp       : ", short.stringof);
    writeln("Länge in Bytes : ", short.sizeof);
    writeln("Mindestwert    : ", short.min);
    writeln("Maximaler Wert : ", short.max);
    writeln("Standardwert   : ", short.init);
    
    writeln();
    
    writeln("Datentyp       : ", long.stringof);
    writeln("Länge in Bytes : ", long.sizeof);
    writeln("Mindestwert    : ", long.min);
    writeln("Maximaler Wert : ", long.max);
    writeln("Standardwert   : ", long.init);
}
---


Macros:
        SUBTITLE=Lösungen zu Fundamentalen typen

        DESCRIPTION=Softwareentwicklung in D Lösung zu: Fundamentalen Typen

        KEYWORDS=programming in d tutorial lösung deutsch fundamentale typen
