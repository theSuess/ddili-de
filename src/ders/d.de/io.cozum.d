Ddoc

$(COZUM_BOLUMU Standard Input und Output Streams)

---
import std.stdio;

void main() {
    stdout.write(1, ",", 2);

    // Wollen wir die Zeile vervollständigen benötigen wir immer noch:
    writeln();
}
---

Macros:
        SUBTITLE=Standard Input und Output Streams Lösungen

        DESCRIPTION=Programming in D übunslösungen: standard input und output streams

        KEYWORDS=programming in d tutorial standard input und output stream lösung
