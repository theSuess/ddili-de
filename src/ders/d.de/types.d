Ddoc

$(DERS_BOLUMU $(IX type) $(IX fundamental type) Fundamentale Datentypen)

$(P
Wir haben bereits gesehn das das Gehirn des Computers die CPU ist. Die meisten Teile eines Programms werden von der CPU erledigt und der Rest auf die anderen Teile des PCs ausgelagert.
)

$(P
Die kleinste Dateneinheit im Computer ist ein $(I bit). Ein Bit kann entweder 0 oder 1 sein.
)

$(P
Da ein Datentyp welcher nur 0 oder 1 speichern kann nur sehr beschränkten Nutzen hat, unterstützt die CPU auch größere Datentypen welche aus mehreren Bits bestehen.
Beispielsweise besteht ein ($I byte) aus 8 Bits. Wenn ein N-Bit Datentyp der effizienteste Datentyp einer CPU ist, ist es eine $(I N-bit CPU). Daher gibt es 32-Bit und 64-Bit Systeme.
)

$(P
Datentypen welche die CPU unterstützt sind aber noch lange nicht genug. Sie können noch immer keine Konzepte wie den $(I Namen eines Schülers) oder eine $(I Spielkarte)
darstellen.
Ebenso sind die Fundamentalen typen nicht genug für komplexere Datenstrukturen. Diese Konzepte müssen vom Programmierer als $(I structs) oder $(I Klassen)  definiert werden.
Dies behandeln wir aber in einem späteren Kapitel.
)

$(P
$(IX bool)
$(IX byte)
$(IX ubyte)
$(IX short)
$(IX ushort)
$(IX int)
$(IX uint)
$(IX long)
$(IX ulong)
$(IX float)
$(IX double)
$(IX real)
$(IX ifloat)
$(IX idouble)
$(IX ireal)
$(IX cfloat)
$(IX cdouble)
$(IX creal)
$(IX char)
$(IX wchar)
$(IX dchar)
Fundamentale Datentypen in D sind sehr ännlich deren anderen Sprachen wie man in der folgenden Tabelle sieht.
)

<table class="full" border="1" cellpadding="4" cellspacing="0"><caption>Fundamentale Datentypen</caption>
	<tr><th scope="col">Type</th> <th scope="col">Definition</th> <th scope="col">Ausgangswert</th>

	</tr>
	<tr>		<td>bool</td>

		<td>Boolscher Wert</td>
		<td>false</td>
	</tr>
	<tr>		<td>byte</td>
		<td>8-Bit mit Vorzeichen</td>
		<td>0</td>

	</tr>
	<tr>		<td>ubyte</td>
		<td>8-Bit ohne Vorzeichen</td>
		<td>0</td>
	</tr>
	<tr>		<td>short</td>

		<td>16-Bit mit Vorzeichen</td>
		<td>0</td>
	</tr>
	<tr>		<td>ushort</td>
		<td>16-Bit ohne Vorzeichen</td>
		<td>0</td>

	</tr>
	<tr>		<td>int</td>
		<td>32-Bit mit Vorzeichen</td>
		<td>0</td>
	</tr>
	<tr>		<td>uint</td>

		<td>32-Bit ohne Vorzeichen</td>
		<td>0</td>
	</tr>
	<tr>		<td>long</td>
		<td>64-Bit mit Vorzeichen</td>
		<td>0L</td>

	</tr>
	<tr>		<td>ulong</td>
		<td>64-Bit ohne Vorzeichen</td>
		<td>0L</td>
	</tr>
	<tr>		<td>float</td>
		<td>32-Bit Gleitkommazahl</td>
		<td>float.nan</td>
	</tr>
	<tr>		<td>double</td>

		<td>64-Bit Gleitkommazahl</td>
		<td>double.nan</td>
	</tr>
	<tr>		<td>real</td>
		<td>Entweder der größte Gleitkommadatentyp des Systems oder double, jenachdem was größer ist</td>

		<td>real.nan</td>
	</tr>
	<tr>		<td>ifloat</td>
		<td>Imaginärer Float Wert</td>
		<td>float.nan * 1.0i</td>
	</tr>

	<tr>		<td>idouble</td>
		<td>Imaginärer Double Wert</td>
		<td>double.nan * 1.0i</td>
	</tr>
	<tr>		<td>ireal</td>
		<td>Imaginärer Real Wert</td>

		<td>real.nan * 1.0i</td>
	</tr>
	<tr>		<td>cfloat</td>
		<td>Komplexe Zahl bestehend aus 2 Floats</td>
		<td>float.nan + float.nan * 1.0i</td>
	</tr>

	<tr>		<td>cdouble</td>
		<td>Komplexe Zahl bestehend aus 2 Doubles</td>
		<td>double.nan + double.nan * 1.0i</td>
	</tr>
	<tr>		<td>creal</td>
		<td>Komplexe Zahl bestehend aus 2 Reals</td>

		<td>real.nan + real.nan * 1.0i</td>
	</tr>
	<tr>		<td>char</td>
		<td>UTF-8 Zeichen</td>
		<td>0xFF</td>
	</tr>

	<tr>		<td>wchar</td>
		<td>UTF-16 Zeichen</td>
		<td>0xFFFF</td>
	</tr>
	<tr>		<td>dchar</td>
		<td>UTF-32 Zeichen und Unicode code point</td>

		<td>0x0000FFFF</td>
	</tr>
	</table>
$(P
Das $(C u) vor manchen Typen zeigt an das sie $(I unsigniert) sind. Das bedeutet das sie $(B kein) Vorzeichen besitzen und deswegen nur Positive Werte annehmen können.
)

$(P
Zusätzlich zu den oben genannten Typen, repräsentiert das Schlüsselwort $(C void) das etwas $(C keinen Typ besitzt).
Die Schlüsselwörter $(C cent) und $(C ucent) sind reserviert für 128-Bit Werte.
)

$(P
Solltest du keinen speziellen Grund haben kannst du so gut wie immer $(C int) für Ganzzahlen und $(C double) für Kommazahlen verwenden.
)

$(P
Folgende Ausdrücke wurden in der Tabelle genannt:
)

$(UL

$(LI
$(B Boolscher Wert:) Das Ergebniss eines logischen Ausdrucks: $(C true) für ($I Wahr), $(C false) für $(I Falsch).
)

$(LI
$(B Gleitkommazahl:) Gleitkommazahlen sind jene Zahlen mit Kommastellen. Die bitanzahl gibt hier an, wie viele Nachkommastellen es geben kann.
)

$(LI
$(IX .nan) $(B nan:) Kurz für "not a number" (=keine Zahl) und wird für ungültige Gleitkommawerte verwendet.
)

)

$(H5 Eigenschaften von Typen)

$(P
In D hat jeder Typ sogenannte $(I Properties) (=Eigenschaften). Auf sie wird mit einem Punkt nach dem Namen zugegriffen.
Beispielsweise wird auf die Property $(C sizeof) von $(C int) mit $(C int.sizeof) zugegriffen.
In diesem Kapitel schauen wir uns nur ein paar der vielen Properties an.
)

$(UL

$(LI $(IX .stringof) $(C .stringof) ist der Name des Typen)

$(LI $(IX .sizeof) $(C .sizeof) Die Länge des Datentypes in Byte. (Um von Bytes auf Bit zu kommen muss man sie mit 8 Multiplizieren)
)

$(LI $(IX .min) $(C .min) ist kurz für "minimum"; Dies ist der kleinstmögliche Wert des Datentypes)

$(LI $(IX .max) $(C .max) ist kurz für "maximum"; Dies ist der größtmöglichste Wert des Datentypes)

$(LI $(IX .init) $(IX initial value) $(IX Standardwert, Datentyp) $(C .init) steht für "initial value" (Standardwert); Dies ist der Wert welcher angenommen wird, wenn bisher nichts zugewiesen ist)

)

$(P
Hier ist ein Programm das die Eigenschaften von $(C int) ausgibt:
)

---
import std.stdio;

void main() {
    writeln("Datentyp       : ", int.stringof);
    writeln("Länge in Bytes : ", int.sizeof);
    writeln("Mindestwert    : ", int.min);
    writeln("Maximaler Wert : ", int.max);
    writeln("Standardwert   : ", int.init);
}
---

$(P
Die Ausgabe des Programms lautet wie folgt:
)

$(SHELL
Datentyp       : int
Länge in Bytes : 4
Mindestwert    : -2147483648
Maximaler Wert : 2147483647
Standardwert   : 0
)

$(H5 $(IX size_t) $(C size_t))

$(P
Ebenso wird dir der Typ $(C site_t) begegnen. $(C size_t) ist jedoch kein Datentyp an sich sondern nur ein alias für einen anderen Typen. Der Name kommt von "size type".
Er wird verwendet um Konzepte wie $(I Größe) oder $(I Anzahl) darzustellen.
)

$(P
$(C size_t) ist größer (oder entspricht) der Anzahl in Bytes welcher dem Programm an Speicher zur verfügung steht. Seine echte größe hängt vom System ab:
$(C uint) auf einem 32-Bit- und $(C ulong) auf einem 64-Bit System. Aus dem Grund ist $(C ulong) größer als $(C size_t) auf einem 32-Bit System.
)

$(P
Du kannst die $(C .stringof) Property verwenden um zu sehen für was $(C size_t) auf deinem System steht:
)

---
import std.stdio;

void main() {
    writeln(size_t.stringof);
}
---

$(P
Auf meinem System ist die Ausgabe wie folgt:
)

$(SHELL
ulong
)

$(PROBLEM_TEK

$(P
Gebe die Eigenschaften von anderen Typen aus.
)

$(P
$(I $(B Zur Information:) Zurzeit kannst du $(C cent) und $(C ucent) noch nicht verwenden. Als einzige Ausnahme hat $(C void) die Properties $(C .min), $(C .max) und $(C .init) nicht.)
)

$(P
$(I Desweiteren ist die $(C .min) Property für Gleitkommazahlen $(I deprecated) und wird bald entfernt.
(Alle Properties sind in der $(LINK2 http://dlang.org/property.html, D property specification)) einzusehen.
Wenn du bei dieser Übung einen Gleitkommatyp verwendest wirst du vom Compiler gewarnt. Verwende stattdessen (wie wir später im $(LINK2 /ders/d.en/floating_point.html, Gleitkomma-Kapitel) sehen werden)
das negativ der $(C .max) Property also $(C -double.max).)
)

)


Macros:
        SUBTITLE=Fundamentale Datentypen

        DESCRIPTION=Die Fundamentalen Datentypen der D Sprache

        KEYWORDS=d programming language tutorial book fundamental types numeric limits zahlen deutsch
