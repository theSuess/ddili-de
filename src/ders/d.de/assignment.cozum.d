Ddoc

$(COZUM_BOLUMU Zuweisung und Auswertungsreihenfolge)

$(P
Die Werte von $(C a), $(C b), und $(C c) sind auf der rechten Seite angeführt. Der Wert, welcher sich ändert, ist hervorgehoben:
)

$(MONO
Anfags             →     a 1, b 2, c unwichtig
c = a              →     a 1, b 2, $(HILITE c 1)
a = b              →     $(HILITE a 2), b 2, c 1
b = c              →     a 2, $(HILITE b 1), c 1
)

$(P
Am Schluss sind die Werte von $(C a) und $(C b) vertauscht.
)

Macros:
        SUBTITLE=Zuweisung und Auswertungsreihenfolge

        DESCRIPTION=Lösungen zur Zuweisung und Auswertungsreihenfolge

        KEYWORDS=programming in d tutorial Zuweisung und Auswertungsreihenfolge lösungen buch german
