Ddoc

$(COZUM_BOLUMU writeln and write)

$(OL

$(LI
Eine möglichkeit ist es, einen neuen Parameter dazwischen zu verwenden:

---
    writeln("Hello world!", " ", "Hallo du!");
---

)

$(LI
$(C write) kann ebenso mehrere Parameter übernehmen:

---
    write("one", " two", " three");
---

)

)

Macros:
        SUBTITLE=writeln und write Lösungen

        DESCRIPTION=Programming in D Übunslösungen: writeln and write

        KEYWORDS=programming in d tutorial writeln und write übungs Lösung
