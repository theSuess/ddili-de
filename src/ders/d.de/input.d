Ddoc

$(DERS_BOLUMU $(IX input) Eingabe aus stdin)

$(P
Alle Daten welche von unseren Programmen gelesen werden müssen in einer Variable gespeichert werden. Beispielsweise muss ein
Programm welches die Anzahl der Schüler einliest diese Zahl speichern. $(C int) wäre ein geeigneter Datentyp
)

$(P
Wie im vorherigem Kapitel erwähnt müssen wir $(C stdout) nicht schreiben da es impliziert wird. Desweiteren wird die Ausgabe als argument übergeben.
$(C write(anzahlSchueler)) reicht also um die Zahl auszugeben. Zusammengefasst heißt das:
)

$(MONO
Stream:      stdout
Operation:   write
Daten:        Der Wert unserer Variable
Ziel:      meistens das Terminal
)

$(P
$(IX readf) Das Gegenteil von $(C write) ist $(C readf). Diese Funktion "liest" vom Terminal. Das $(C f) stammt von Formatted was bedeutet das die Eingabe
in einem spezifischem Format erfolgen muss.
)

$(P
$(C stdin) ist der standard input stream.
)

$(P
Ein teil unserer Funktion fehlt jedoch:
)

$(MONO
Stream:      stdin
Operation:   readf
Daten:        irgendwelche daten
Ziel:      ?
)

$(P
Der Speicherort der Daten wird über die Addresse einer Variable festgelegt. Die Addresse ist der exakte Ort im Speicher wo der Wert gespeichert ist.
)

$(P
$(IX &, addresse von) In D symbolisiert das $(C &) Zeichen vor einer Variable ihre Addresse.
Die Addresse von $(C anzahlSchueler) können wir mit $(C &amp;anzahlSchueler) erfahren.
Gelesen wird das ganze als "Die addresse von $(C anzahlSchueler)". Damit haben wir unser Ziel gefunden:
)

$(MONO
Stream:      stdin
Operation:   readf
Daten:        irgendwelche daten
Ziel:      Addresse von anzahlSchueler
)

$(P
Das $(C &) vor einem Namen bedeutet $(I zeigt) auf das, was den Namen representiert. Dies ist das Konzept von Pointern was wir aber in einem späterem Kapitel besonders
behandeln.
)

$(P
Der erste Parameter ist das Format, was uns jetzt aber noch nicht interessiert (wir schauen uns dies in einem eigenem Kapitel genau an).
Lass uns für jetzt einfach sagen das das erste Argument von $(C readf) immer $(STRING "%s") ist.
)

---
    readf("%s", &anzahlSchueler);
---

$(P
$(I $(B Zur Information:) Wie unten noch erklärt muss in den meisten fällen noch ein abstand davor: $(STRING "&nbsp;%s").)
)

$(P
$(STRING "%s") gibt an, dass die Daten automatisch in das richtige format eingelesen werden sollen. Beispielsweise werden die Chars '4' und '2' automatisch in den
$(C int) 42 konvertiert.
)

$(P
Das Programm unten fragt den Nutzer jetzt wie viele Schüler die Schule besuchen. Du musst die Enter-Taste drücken
um die Eingabe zu bestätigen.
)

---
import std.stdio;

void main() {
    write("Wie viele Schüler gibt es? ");

    // Die Variable in welcher wir die Zahl speichern
    int anzahlSchueler;

    // Die eingabe lesen
    readf("%s", &anzahlSchueler);

    writeln("Verstanden! Es sind ", anzahlSchueler, " Schüler");
}
---

$(H5 $(IX %s, mit Leerzeichen) $(IX Leerzeichen) Leerzeichen ignorieren)

$(P
Auch der Druck der Enter-Taste ist als spezieller Code gespeichert und befindet sich im $(C stdin) stream. Das ist nützlich um
festzustellen ob die Eingabe auf einer oder mehreren Zeilen erfolgte.
)

$(P
Auch wenn sie manchmal nützlich sind, sind diese Codes meist nicht wichtig für das Programm und müssen ausgefiltert werden.
Geschieht das nicht kann es sein das die Eingabe $(I blockiert) wird, was das weitere lesen verhindert.
)

$(P
Um uns das in der Praxis anzuschauen, lass uns auch noch die Zahl der Lehrer einlesen:
)

---
import std.stdio;

void main() {
    write("Wie viele Schüler gibt es? ");
    int anzahlSchueler;
    readf("%s", &anzahlSchueler);

    write("Wie viele Lehrer gibt es? ");
    int anzahlLehrer;
    readf("%s", &anzahlLehrer);

    writeln("Verstanden! Es sind ", anzahlSchueler, " Schüler",
            " und ", anzahlLehrer, " Lehrer.");
}
---

$(P
Leider kann das Programm den Enter Code nicht annehmen wenn es einen $(C int) erwartet:
)

$(SHELL
Wie viele Schüler gibt es?  100
Wie viele Lehrer gibt es? 20
  $(SHELL_NOTE_WRONG Hier hängt sich das Programm auf)
)

$(P
Auch wenn der nutzer 20 eingibt, ist der Enter Code aus der eingabe von 100 noch immer im stream und blockeirt ihn.
Der stream sieht wie folgt aus.
)

$(MONO
100$(HILITE [EnterCode])20[EnterCode]
)

$(P
Der blockierende Code ist hier hervorgehoben.
)

$(P
Die Lösung dafür ist ein Leerzeichen vor dem $(STRING %s) einzufügen. Das gibt an dass der Enter-Code vor der eingabe unwichtig ist. Ausgeschrieben heist das :$(STRING "&nbsp;%s").
Leerzeichen geben an dass alle unsichtbare Zeichen unwichtig sind. Dazu zählen: Enter,Tab und das Leerzeichen selber.
)

$(P
Grob gesprochen kannst du immer $(STRING "&nbsp;%s") verwenden. Das Programm von oben funktioniert wenn wir folgende Änderungen vornehmen:
)

---
// ...
    readf(" %s", &anzahlSchueler);
// ...
    readf(" %s", &anzahlLehrer);
// ...
---

$(P
The output:
)

$(SHELL
Wie viele Schüler gibt es?  100
Wie viele Lehrer gibt es?  20
Got it: There are 100 students and 20 teachers.
)

$(H5 Zusatzinformation)

$(UL

$(LI
$(IX Kommentar) $(IX /*) $(IX */) Zeilen die mit $(COMMENT //) beginnen
sind nützlich um Kommentare (einzeilig) in den Quellcode einzufügen.
Um mehrere Zeilen zu kommentieren musst du den Text mit $(COMMENT /*) und $(COMMENT */) umschließen.


$(P
$(IX /+) $(IX +/) Um in Kommentare zu verschachteln verwendet man $(COMMENT /+) und $(COMMENT +/):

)

---
    /+
     // Einzeiliger Kommentar

     /*
		Kommentar
		mit mehreren
		Zeilen
      */

      /+
        Verschachtelte /+ Kommentare +/
      +/

	  Ein Kommentarblock welcher andere enthält.
     +/
---

)

$(LI
Die meisten Leerzeichen im Quellcode sind unwichtig. Es ist üblich, längere Ausdrücke in mehrere Zeilen zu schreiben und Leerzeichen zu verwenden um es schöner zu gestalten.
Solange jedoch die Syntax-Regeln befolgt werden kann das Programm auch ohne Leerzeichen geschrieben werden.

---
import std.stdio;void main(){writeln("Schwerer zu lesen!");}
---

$(P
Es kann schwer sein solch einen Code zu lesen.
)

)

)

$(PROBLEM_TEK

$(P
Gib bei unserem Programm einen Wert ein, welcher keine Zahl ist. Beobachte wie was passiert.
)

)


Macros:
        SUBTITLE=Eingabe aus dem Standard-Input lesen
        DESCRIPTION=Eingabe aus dem Standard-Input lesen
        KEYWORDS=d programming language tutorial buch eingabe stdin

MINI_SOZLUK=
