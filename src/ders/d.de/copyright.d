Ddoc

<div class="imprint">

$(P
<span class="copyright_title">Softwareentwicklung in D, First Edition</span>
)

$(BR)

$(P
Revision: $(LINK2 https://bitbucket.org/acehreli/ddili, <xi:include href="pdf_surum.txt" parse="text"/>)
)

$(BR)

$(P
Die aktuellste Version befindet sich $(LINK2 http://ddili.org/ders/d.de, online).
)

$(BR)
$(BR)

$(P
Copyleft (ɔ) 2009-2015 Ali Çehreli
)

$(BR)

$(P
<img alt="Creative Commons License" style="border-width:0" src="$(ROOT_DIR)/image/by-nc-sa.png" width="88"/>
)
$(BR)
<p style="margin-left:1em; text-indent:0em">
Dieses Buch ist lizensiert unter der Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. Um sie einzusehen besuche http://creativecommons.org/licenses/by-nc-sa/4.0/.
</p>

$(BR)
$(BR)

$(P
Übersetzt ins Deutsche von $(LINK2 http://thesuess.github.io, Dominik Süß)
)

$(BR)

$(P
Covergestaltung von $(LINK2 http://izgiyapici.com, İzgi Yapıcı)
)

$(BR)

$(P
Coverillustration von $(LINK2 mailto:sarah@reeceweb.com, Sarah Reece)
)

$(BR)

$(P
Veröffentlicht von $(LINK2 mailto:acehreli@yahoo.com, Ali Çehreli)
)

$(BR)
$(BR)

$(P
Schriftarten:
)
<ul class="font_list">
<li style="font-family: serif;">
Andada von Carolina Giovagnoli für Huerta Tipográfica
</li>
<li style="font-family: sans-serif;">
Open Sans von Steve Matteson
</li>
<li style="font-family: monospace;">
DejaVu Mono von DejaVu Fonts
</li>
</ul>

$(BR)
$(P
Die PDF version wurde mit Prince XML generiert
)
$(P
Andere EBook versionen wurden mit Calibre erstellt
)
$(BR)
$(P
Gedruckt von IngramSpark
)
$(P
ISBN 978-0-692-52957-7
)
$(BR)
$(P
oder von CreateSpace
)
$(P
ISBN 978-1515074601
)

</div>

Macros:
        SUBTITLE=Copyleft

        DESCRIPTION=Die copyleft Seite von Softwareentwicklung in D

        KEYWORDS=copyright
