Ddoc

$(DERS_BOLUMU $(IX kompilierung) Vom Quellcode zum Programm)

$(P
Wir haben bereits gesehen das die zwei meistverwendetsten Werkzeuge der $(I Text-Editor) und der $(I Compiler) sind.
)

$(P
Das Konzept und Prinzip des Kompilers muss verstanden werden um in Kompilierten Sprachen wie D zu schreiben.
)

$(H5 $(IX Maschinencode) Maschinencode)

$(P
$(IX CPU) $(IX Prozessor) Das Gehirn des Computers ist der Prozessor (kurz CPU für $(I central processing unit)).
Der CPU zu sagen was sie tun soll ist das $(I Programmieren) und die Instruktionen die verwendet werden ist der $(I Maschinencode).
)

$(P
Die meisten CPU Architekturen verwenden Code welcher spezifisch für genau diesen Prozessor ist.
Diese Instruktionen sind Hardwaremäßig festgelegt und werden auf der niedrigsten Ebene in elektrische Signale umgewandelt.
Programme direkt in Maschinencode zu schreiben, wie es früher geschah, ist sehr sehr schwer.
)

$(P
Maschinencode besteht aus speziellen Zahlen welche verschiedenen Operationen entsprechen. Beispielsweise, kann die nummer 4 die Operation des Laden entsprechen, die 5 des Speicherns und die 6 des Erhöhens.
Angenommen die linken 3 Bit sind die Operation und die 5 übrigen sind die Werte, kann ein Maschinencode-Programm so ausschauen:
)

$(MONO
$(B
Operation   Wert            Bedeutung)
   100      11110        LOAD      11110
   101      10100        STORE     10100
   110      10100        INCREMENT 10100
   000      00000        PAUSE
)

$(P
Weil er so nahe an der Hardware ist, ist der Maschinencode nicht sehr geeignet um Kompliziertere Konzepte wie eine Spielkarte darzustellen.
)

$(H5 $(IX Programmiersprache) Programmiersprache)

$(P
Programmiersprachen wurden entwickelt um eine effiziente Möglichkeit zu bieten, genau solche Konzepte darzustellen. Programmiersprachen müssen nicht mit Hardwarebeschränkungen umgehen.
Sie sind leichter zu verstehen da sie näher an der natürlichen Sprache liegen.
)

$(MONO
if (eine_karte_wurde_gespielt()) {
   zeige_die_karte_an();
}
)

$(P
Programmiersprachen unterliegen jedoch viel strengeren Regeln als gesprochene Sprache.
)

$(H5 $(IX interpreter) Interpreter)

$(P
Ein Interpreter ist ein Tool/Programm das die Anweisungen direkt aus dem Quellcode liest und sie ausführt. Bei dem Beispiel von oben würde ein Interpreter als erstes
$(C eine_karte_wurde_gespielt()) ausführen und wenn die Anweisung wahr ist $(C zeige_die_karte_an()) aufrufen. Programmieren in einer Interpreter-Sprache
besteht genau aus zwei Schritten: den Code schreiben und ihn an den Interpreter übergeben.
)

$(P
Der Interpreter muss die Anweisungen jedes mal lesen, wenn das Programm ausgeführt wird. Aus dem Grund sind Programme, mit einem Interpreter ausgeführt, normalerweise langsamer als
eine Equivalente Version, welche mit einem Compiler erstellt wurde. Interpreter können nur wenig Analyse des Codes durchführen und sind deswegen schwerer zu $(I debuggen) da man
Fehler erst zur Laufzeit feststellen kann.
)

$(P
Sprachen wie Perl, Python und Ruby wurden entwickelt um möglichst Flexibel und Dynamisch zu sein. Diese Sprachen werden normalerweise $(I interpretiert).
)

$(H5 $(IX Compiler) Compiler)

$(P
Ein Compiler ist ein anderes Tool welches den Quellcode liest. Anders wie bei einem Interpreter führt er den Code nicht direkt aus. Er erstellt eine ausführbare Datei, meistens Maschinencode.
Die Entwicklung mit einem Compiler erfolgt in drei Schritten: Code schreiben, Kompilieren, Ausführen.
)

$(P
Anders wie beim Interpreter liest der Compiler die Anweisungen nur einmal und zwar dann, wenn er das Programm erstellt. Genau aus diesem Grund sind kompilierte Programme schneller.
Compiler können den Code gut analysieren und verbessern was die Geschwindigkeit noch mehr erhöht und schon Fehler vor dem ausführen entdeckt. Problematisch
bei Compilern ist, dass ein Programm erstellt für eine Plattform nicht auf allen laufen wird. Des weiteren fügt der schritt der Kompilation eine weitere Zeitverzögerung ein.
Kompilierte Sprachen sind meist weniger dynamisch oder Flexibel als Interpretersprachen.
)
$(P
Aus Gründen der Sicherheit und Performance wurden manche sprachen wie Ada, C, C++ und D für Compiler entwickelt.
)

$(H6 $(IX Fehler, Kompilierung) $(IX Compilerfehler) Compilerfehler)

$(P
Da der Kompiler nur nach den Regeln der Sprache handeln kann, bricht er ab sobald er eine $(I ungültige) Anweisung erhält. Üngültige Anweisung sind solche, welche der Sprachspezifikation nicht entsprechen.
Probleme wie nichtgeschlossene Klammerpaare, fehlende Semikolons oder ein falschgeschriebenes Schlüsselwort sind alles Ursachen für Compilerfehler.
)

$(P
Der Compiler kann aber nicht nur Fehler erzeugen. Er kann ebenso $(I Warnungen) anzeigen. Diese erscheinen wenn ein verdächtiger Block Code gefunden wurde, welcher aber nicht Fatal ist.
Warnungen zeigen jedoch oft auch Fehler oder schlechten Stil an. Deshalb ist es normal, alle Warnungen als Fehler zu behandeln. Die $(C dmd) Option dafür ist $(C -w).
)

$(Ergin)

Macros:
        SUBTITLE=Compiler

        DESCRIPTION=The introduction of the compiler and compiled languages

        KEYWORDS=d programming language tutorial book
