Ddoc

$(DERS_BOLUMU $(IX Variablen) Variablen)

$(P
Konkrete Konzepte in einem Programm sind sogenannte $(I Variablen). Ein Wert wie die $(I Temperatur) aber auch komplizierte Objekte wie ein $(I Automotor) können Variablen sein.
)

$(P
Die Hauptaufgabe einer Variable ist es einen Wert zu representieren. Der Wert einer Variable ist immer der letzte Wert welcher ihr zugewiesen wurde.
Da jeder Wert immer einen Typen haben muss hat auch jede Variable einen Typen. Die meisten Variablen haben Namen, manche bleiben jedoch annonym.
)

$(P
Als beispiel nehmen wir als Variable die Anzahl der Schüler an einer Schule. Da es keine halben Schüler geben kann ist $(C int) ein
geeigneter Datentyp und $(C anzahlSchueler) ein sehr beschreibender Variablenname.
)

$(P
D's Syntax besagt das eine Variable wie folgt definiert wird: zuerst der Typ, dann der Name. Der name der Variable representiert ihren Wert.
)

---
import std.stdio;

void main() {
    // Dies ist die sogenannte definition einer
	// Variable. Sie gibt an das die Variable
	// anzahlSchueler eine Ganzzahl ist.
    int anzahlSchueler;

	// Über den Namen haben wir zugriff auf den Wert
    writeln("Es gibt ", anzahlSchueler, " Schüler");
}
---

$(P
Die Ausgabe ist:
)

$(SHELL
Es gibt 0 Schüler
)

$(P
In der Ausgabe macht sich bemerkbar das der Wert unserer Variable 0 ist obwohl wir ihr nie etwas zuweisen. Denken wir ein paar
Kapitel zurück, so fällt uns auf das dies der $(C .init) Wert von $(C int) ist.
)

$(P
Da $(C anzahlSchueler) nicht unter Hochkommas steht ist die Ausgabe nicht: "Es gibt anzahlSchueler Schüler".
)

$(P
Die Werte einer Variable können mit dem $(C =) Operator verändert werden. Der Operator weist einer Variable einen neuen Wert zu.
Deshalb wird er auch der $(I Zuweisunsoperator) genannt.
)

---
import std.stdio;

void main() {
    int anzahlSchueler;
    writeln("Es gibt ", anzahlSchueler, " Schüler");

    // Wir weisen der Variable anzahlSchueler
	// den Wert 200 zu:
    anzahlSchueler $(HILITE =) 200;
    writeln("Es gibt jetzt ", anzahlSchueler, " Schüler");
}
---

$(SHELL
Es gibt 0 Schüler
Es gibt jetzt 200 Schüler
)

$(P
Wenn der Wert der Variable schon bekannt ist wenn wir sie definieren, so können wir das gleich in einer Zeile machen.
Dies sollte man immer machen da man so das Risiko minimiert dass die Variable verwendet wird bevor sie überhaupt einen
sinnvollen Wert hat.
)

---
import std.stdio;

void main() {
	// Wir definieren die Variable und weisen
	// ihr gleich einen Wert zu:
    int anzahlSchueler = 100;

    writeln("Es gibt ", anzahlSchueler, " Schüler");
}
---

$(SHELL
Es gibt 100 Schüler
)

$(PROBLEM_TEK

$(P
Definiere zwei Variablen um "Es ist 14 Uhr und ich habe 4.5€ bei mir." auszugeben. Ein geeigneter Gleitkommatyp ist
beispielsweise $(C double).
)

)


Macros:
        SUBTITLE=Variablen

        DESCRIPTION=Variablen in der D Sprache

        KEYWORDS=d programming language tutorial buch variablen deutsch

MINI_SOZLUK=
