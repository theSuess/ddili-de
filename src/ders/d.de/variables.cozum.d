Ddoc

$(COZUM_BOLUMU Variablen)

---
import std.stdio;

void main() {
    int uhrzeit = 14;
    double geld = 4.5;

    writeln("Es ist ", uhrzeit,
            " Uhr und ich habe ", geld ,"€ bei mir.");
}
---

Macros:
        SUBTITLE=Variablen Lösung

        DESCRIPTION=Lösung zum Kapitel Variablen

        KEYWORDS=programming in d tutorial variablen lösung
