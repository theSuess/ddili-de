Ddoc

$(DIV_CLASS preface,

$(DERS_BOLUMU_CLASS preface, Einführung)

$(P
D ist eine Programmiersprache welche eine weite Spanne von Programmiertechniken (low bis high level) enthält. D legt besonders Wert auf Speichersicherheit, Programm Richtigkeit und Sachlichkeit.
)

$(P
Das Ziel dieses Buches ist es, die Grundlagen, sowie erweiterte Themen, der Softwareentwicklung beizubringen. Beherrschung von anderen Programmiersprachen ist keine Vorraussetzung, dieses
Buch beginnt bei den absoluten Grundlagen.
)

$(P
Damit dieses Buch dir auch wirklich nützlich ist benötigst du eine Entwicklungsumgebung in welcher du deine Programme schreibst und erstellst. Diese muss mindestens einen D-Compiler und einen
Text Editor enthalten. Im nächsten Kapitel lernen wir wie wir einen Compiler installieren und Programme erstellen.
)

$(P
Jedes Kapitel basiert auf den Konzepten der vorhergehenden und führt dabei so wenig neue Konzepte ein wie möglich. Ich rate dir deshalb vor allem als Beginner keine Kapitel auszulassen.
Auch wenn das Buch überwiegend für Anfänger geschreiben ist, deckt es fast alle Eigenschaften von D ab und kann deshalb für fortgeschrittene als Referenz verwendet werden.
)

$(P
Manche Kapitel beinhalten Übungen zum üben und veranschaulichen der Konzepte. Mithilfe der Lösungen kannst du dann dein Ergebniss mit meinem Vergleichen.
)

$(P
Softwareentwicklung ist eine zufriedenstellende Kunst welche permanentes Entdecken und Lernen von neuen Werkzeugen, Techniken und Konzepten belohnt. Ich bin mir
Sicher du wirst das programmieren in D genau so wie ich genießen. Programmieren zu lernen ist leichter und lustiger wenn man es mit anderen macht.
Hilfreich dafür ist das $(LINK2 http://forum.dlang.org/group/digitalmars.D.learn/, D.learn ) Forum.
)

$(P
Dieses Buch ist auch auf $(LINK2 http://ddili.org/ders/d/, Türkish), $(LINK2 http://ddili.org/ders/d.en/, Englisch) und $(LINK2 http://dlang.unix.cat/programmer-en-d/, Französisch) verfügbar.
)

$(H5_FRONTMATTER Dank an die Helfer)

$(P
Ich danke aus ganzem Herzen folgenden Leuten ohne welche dieses Buch nicht möglich geworden wäre:
)

$(P
Mert Ataol, Zafer Çelenk, Salih Dinçer, Can Alpay Çiftçi, Faruk Erdem Öncel, Muhammet Aydın (aka Mengü Kağan), Ergin Güney, Jordi Sayol, David Herberth, Andre Tampubolon, Gour-Gadadhara Dasa, Raphaël Jakse, Andrej Mitrović, Johannes Pfau, Jerome Sniatecki, Jason Adams, Ali H. Çalışkan, Paul Jurczak, Brian Rogoff, Михаил Страшун (Mihails Strasuns), Joseph Rushton Wakeling, Tove, Hugo Florentino, Satya Pothamsetti, Luís Marques, Christoph Wendler, Daniel Nielsen, Ketmar Dark, Pavel Lukin, Jonas Fiala, Norman Hardy, Rich Morin, Douglas Foster, Paul Robinson, Sean Garratt, Stéphane Goujet, Shammah Chancellor, Steven Schveighoffer, Robbin Carlson, Bubnenkov Dmitry Ivanovich, Bastiaan Veelo, Stéphane Goujet, Olivier Pisano, Dave Yost, Tomasz Miazek-Mioduszewski, Gerard Vreeswijk, Justin Whear, Gerald Jansen, Sylvain Gault und Dominik Bartosch.
)

$(P
Danke auch an Luís Marques welcher, mit seiner harten Arbeit, jedes Kapitel nocheinmal verbessert hat. Solltest du auch nur irgendetwas an diesem Buch nützlich finden, liegt
das warscheinlich an seiner geschickten Bearbeitung.
)

$(P
Danke an Luís Marques, Steven Schveighoffer, Andrej Mitrović, Robbin Carlson, und Ergin Güney für ihre Vorschläge welche dieses Buch in korrektes Englisch verwandelten, was es wiederum möglich machte
das Ganze ins Deutsche zu übersetzen.
)

$(P
Auch einen großen Dank an die gesamte D Community welche meinen Ehrgeiz und meine Motivation hoch hielten. D hat eine wunderbare community von wunderbaren Menschen wie bearophile und Kenji Hara.
)

$(P
Ebru, Damla und Derin, danke dass ihr mich unterstützt habt während ich mich in den Kapiteln verlor.
$(BR)
$(BR)
Ali Çehreli$(BR)
Mountain View, $(I November 2015)
)

)

Macros:
    SUBTITLE = Einführung
    DESCRIPTION=
    KEYWORDS=
